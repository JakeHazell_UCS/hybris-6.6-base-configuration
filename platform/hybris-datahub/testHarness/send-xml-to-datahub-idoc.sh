#/bin/sh
FILE_TO_SEND=$1
DATAHUB_ENDPOINT=http://localhost:8080/datahub-webapp/v1/idoc/receiver
DATAHUB_USERNAME=admin
DATAHUB_PASSWORD=nimda
echo
echo "SENDING FILE TO DATAHUB:"
echo "  file: $FILE_TO_SEND "
echo "  endpoint: $DATAHUB_ENDPOINT"
echo "  client_identifier: $DATAHUB_CLIENT_IDENTIFIER"
echo
curl -X POST --user $DATAHUB_USERNAME:$DATAHUB_PASSWORD --header "Content-Type:application/xml;charset=UTF-8;" --data "@$FILE_TO_SEND" $DATAHUB_ENDPOINT